/* use strict */
var app = angular.module('MyApp')
app.controller('demoCtrl', ['$scope','applicationFactory',function($scope, applicationFactory) {

    var vm = this;
    vm.token="";
    vm.player1Hand=[];
    vm.player2Hand=[];
    vm.result1="";
    vm.result2="";
    vm.resultDesc1="";
    vm.resultDesc2="";
    var handPlayer1 = [];
    var handPlayer2 = [];
    var hand1=null;
    vm.playerWin="";
    initPlayerHand();

    function initPlayerHand(){
        applicationFactory.getToken().then(function(response){
            vm.token=response.data;
            applicationFactory.getHand(vm.token).then(function(response){
                vm.player1Hand=response.data;
                var cardSymbols = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"];
                var suitSymbols = ["♠", "♣", "♥", "♦"];
                var nameSuitSymbols = ["spades", "clubs", "hearts", "diamonds"];
            
                var tableHTML = '';
                for (var j = 0; j < 5; j++) {
                    tableHTML += '<select style = "padding:3px;font:normal 32px arial;margin:10px" id = "card1_' + j + '">';
                    tableHTML += '<option value = "' + cardSymbols[cardSymbols.indexOf(vm.player1Hand[j].number) % 13] +vm.player1Hand[j].suit.charAt(0)+ '">' + cardSymbols[cardSymbols.indexOf(vm.player1Hand[j].number) % 13] + " " + suitSymbols[Math.floor(nameSuitSymbols.indexOf(vm.player1Hand[j].suit))] + '</option>';
                    tableHTML += '</select>';
                }
                document.getElementById("table1").innerHTML = tableHTML;
                checkHand1();
            });
            
        },function(){
            initPlayerHand();
        });
    }

    function initPlayer2Hand(){
        applicationFactory.getToken().then(function(response){
            vm.token=response.data;
            applicationFactory.getHand(vm.token).then(function(response){
                vm.player2Hand=response.data;
                var cardSymbols = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"];
                var suitSymbols = ["♠", "♣", "♥", "♦"];
                var nameSuitSymbols = ["spades", "clubs", "hearts", "diamonds"];
            
                var tableHTML = '';
                for (var j = 0; j < 5; j++) {
                    tableHTML += '<select style = "padding:3px;font:normal 32px arial;margin:10px" id = "card2_' + j + '">';
                    tableHTML += '<option value = "' + cardSymbols[cardSymbols.indexOf(vm.player2Hand[j].number) % 13] +vm.player2Hand[j].suit.charAt(0)+ '">' + cardSymbols[cardSymbols.indexOf(vm.player2Hand[j].number) % 13] + " " + suitSymbols[Math.floor(nameSuitSymbols.indexOf(vm.player2Hand[j].suit))] + '</option>';
                    tableHTML += '</select>';
                }
                document.getElementById("table2").innerHTML = tableHTML;
                checkHand2();
            });
            
        },function(){
            initPlayer2Hand();
        });
    }
    function checkHand1() {
        for (var i = 0; i < 5; i++) {
            handPlayer1[i] = document.getElementById("card1_" + i).value;
        }
        hand1 = Hand.solve(handPlayer1);
        vm.result1=hand1.name;
        vm.resultDesc1=hand1.descr;
        initPlayer2Hand();
    }
    function checkHand2() {
        for (var i = 0; i < 5; i++) {
            handPlayer2[i] = document.getElementById("card2_" + i).value;
        }
        hand2 = Hand.solve(handPlayer2);
        vm.result2=hand2.name;
        vm.resultDesc2=hand2.descr;
        playerWinner();
    }
    function playerWinner(){
        var win=Hand.winners([hand1, hand2]);
        vm.playerWin = win[0].descr;
    }
    vm.jugar=function(){
        initPlayerHand();
    }
}]);