var app = angular.module('MyApp', [])
app.factory('applicationFactory', ['$http', function($http) {
    
    var getToken = function() {
        return $http.post("https://services.comparaonline.com/dealer/deck");
    };
    var getHand = function(token) {
        return $http.get("https://services.comparaonline.com/dealer/deck/"+token+"/deal/5");
    };

    return {
        getToken: getToken,
        getHand:getHand
    };
    
}]);